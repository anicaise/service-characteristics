CPU:
    subClassOf: ['gax-trust-framework:HardwareSpec']
    prefix: 'gax-trust-framework'
    attributes:
    -   title: 'cpuArchitecture'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:string'
        valueIn: ['x86', 'x86-64', 'RISC-V', 'Generic']
        cardinality: '0..1'
        description: 'Basic CPU architecture.'
        exampleValues: [ 'x86', 'x86-64', 'RISC-V', 'Generic']

    -   title: 'cpuGeneration'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:string'
        valueIn: [ 'Skylake-Server-v4', 'Icelake-Server-v4', 'Cascadelake-Server-v4', 'EPYC-Milan-v1', 'EPYC-Rome-v2' ]
        cardinality: '0..1'
        description: 'CPU instruction set generation. Determines basic feature set and migration compatibility.'
        exampleValues: [ 'Skylake-Server-v4', 'Icelake-Server-v4', 'Cascadelake-Server-v4', 'EPYC-Milan-v1', 'EPYC-Rome-v2' ]

    -   title: 'cpuFlag'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:string'
        cardinality: '0..*'
        description: 'CPU flags as documented by lscpu and defined in https://github.com/torvalds/linux/blob/master/tools/arch/x86/include/asm/cpufeatures.h .'
        exampleValues:  ['fpu', 'vme', 'de', 'pse', 'sse', 'sse2', 'ht', 'vmx', 'smx', 'sse4_1', 'sse4_2', 'avx', '3dnowprefetch', 'ibrs_enhanced', 'ept_ad', 'sgx', 'sgx_lc', 'md_clear', 'arch_capabilities', ... ]

    -   title: 'smtIsEnabled'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:boolean'
        cardinality: '0..1'
        description: 'Is simultaneous multithreading (SMT) or hyper threading (HT) active in this CPU? Default False.'
        exampleValues: ['true', 'false']

    -
        title: 'numberOfSockets'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:integer' ## > 0
        cardinality: '0..1'
        description: 'Number of CPU Sockets'
        exampleValues: [ '1', '2', '4']

    -   title: 'numberOfCores'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:integer'
        minValue: '1'
        cardinality: '0..1'
        description: 'Number of Cores of the CPU'
        exampleValues: [ '4', '6', '8', '12', '24' ]

    -   title: 'numberOfThreads'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:integer'
        minValue: '1'
        cardinality: '0..1'
        description: 'Number of Threads of the CPU'
        exampleValues: [ '8', '12', '24' ]

    -   title: 'baseFrequency'
        prefix: 'gax-trust-framework'
        dataType: 'gax-trust-framework:Measure'
        cardinality: '0..1'
        description: 'Frequency of the CPU'
        exampleValues: [ 'A structure object of type measure, e.g. measure:value=3.0 and measure:unit=GHz' ]

    -   title: 'boostFrequency'
        prefix: 'gax-trust-framework'
        dataType: 'gax-trust-framework:Measure'
        cardinality: '0..1'
        description: 'Boost frequency of the CPU'
        exampleValues: [ 'A structure object of type measure, e.g. measure:value=4.0 and measure:unit=GHz' ]

    -   title: 'lastLevelCacheSize'
        prefix: 'gax-trust-framework'
        dataType: 'gax-trust-framework:Measure'
        cardinality: '0..1'
        description: 'Last Level Cache size of the CPU'
        exampleValues: [ 'A structure object of type measure, e.g. measure:value=38 and measure:unit=MB' ]

    -   title: 'socket'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:string'
        cardinality: '0..1'
        description: 'Socket the CPU fits into.'
        exampleValues: [ 'FCLGA3647' ]
    -
        title: 'tdp'
        prefix: 'gax-trust-framework'
        dataType: 'gax-trust-framework:Measure'
        cardinality: '0..1'
        description: 'CPU Thermal Design Power - ref : https://en.wikipedia.org/wiki/Thermal_design_power'
        exampleValues: [ 'A structure object of type measure, e.g. measure:value=100 and measure:unit=W' ]

    - 
        title: 'defaultOverbookingRatio'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:float'
        minValue: '1'
        cardinality: '0..1'
        description: 'a dimensionless value larger or equal to 1.0 describing the default maximum number of workloads scheduled on this CPU simultaneously'
        exampleValues: [ '1.0' ]

    - 
        title: 'supportedOverbookingRatio'
        prefix: 'gax-trust-framework'
        dataType: 'xsd:float'
        minValue: '1'
        cardinality: '0..*'
        description: 'several dimensionless values larger or equal to 1.0 describing the available scheduler settings for the numer of simultaneously scheduled workloads on this CPU'
        exampleValues: [ '1.0' ]
