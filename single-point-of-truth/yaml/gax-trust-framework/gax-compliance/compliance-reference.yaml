ComplianceReference:
  prefix: "gax-trust-framework"
  abstract: false
  subClassOf: []
  attributes:
    - title: "hasReferenceUrl"
      prefix: "gax-trust-framework"
      dataType: "xsd:anyURI"
      cardinality: "1..1"
      description: "URI to reference the content of the compliance reference in a single PDF file"
      exampleValues: ["https://www.iso.org/iso-27001.pdf"]

    - title: "hasSha256"
      prefix: "gax-trust-framework"
      dataType: "xsd:string"
      cardinality: "1..1"
      description: "SHA256 of PDF document referenced by hasReferenceUrl."
      exampleValues:
        ["ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad"]
    - title: "hasComplianceReferenceTitle"
      prefix: "gax-trust-framework"
      dataType: "xsd:string"
      cardinality: "1..1"
      description: "Name of the Compliance Reference"
      exampleValues: ["ISO 27001"]
    - title: "hasDescription"
      prefix: "dct"
      dataType: "xsd:string"
      cardinality: "0..1"
      description: "A description in natural language"
      exampleValues: ["Information security management system standards."]
    - title: "hasComplianceReferenceManager"
      prefix: "gax-trust-framework"
      dataType: "gax-trust-framework:ComplianceReferenceManager"
      cardinality: "1..1"
      description: "ID of Participant (self-description) in charge of managing this Compliance Reference"
      exampleValues:
        [
          "https://company-a.com/self-descriptions/compliance-ref-manager.jsonld",
        ]
    - title: "hasVersion"
      prefix: "gax-trust-framework"
      dataType: "xsd:string"
      cardinality: "0..1"
      description: "versioning according to [semver](https://semver.org/)."
      exampleValues: ["22/04 or 2022/04/12 or 1.5.34 or built 240344"]

    - title: "cRValidFrom"
      prefix: "gax-trust-framework"
      dataType: "xsd:dateTime"
      cardinality: "0..1"
      description: "Indicates the first date when the compliance reference goes into effect"
      exampleValues: ["22/04 or 2022/04/12 or 1.5.34 or built 240344"]

    - title: "cRValidUntil"
      prefix: "gax-trust-framework"
      dataType: "xsd:dateTime"
      cardinality: "0..1"
      description: "Indicates the last date when the compliance reference is no more into effect"
      exampleValues: ["22/04 or 2022/04/12 or 1.5.34 or built 240344"]

    - title: "hasComplianceCertificationSchemes"
      prefix: "gax-trust-framework"
      dataType: "gax-trust-framework:ComplianceCertificationScheme"
      cardinality: "1..*"
      description: "One or more schemes that grants certification. This list is managed by a reference manager."
      exampleValues: ["did:web:compliance.cispe.org/coc/gdpr/thirdparty#1"]
