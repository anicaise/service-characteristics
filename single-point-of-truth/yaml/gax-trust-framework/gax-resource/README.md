# The _gax-resource_ Folder
This folder defines the top-level class _Resource_ and contains subclasses of _Resource_ to describe artefacts in the Trust Framework with resource character.